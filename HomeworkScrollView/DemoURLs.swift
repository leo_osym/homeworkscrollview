//
//  DemoURLs.swift
//  HomeworkScrollView
//
//  Created by user153878 on 4/17/19.
//  Copyright © 2019 leo_osym. All rights reserved.
//

import Foundation

struct DemoURLs
{    
    static let andromeda = URL(string:
        "https://img3.goodfon.ru/original/3200x2000/6/f8/galaktika-andromedy.jpg")
    
    static var NASA: Dictionary<String,URL> = {
        let NASAURLStrings = [
            "Carina" : "https://www.nasa.gov/sites/default/files/images/596134main_5755295437_18614bc380_o_full.jpg",
            "Helix" : "https://www.nasa.gov/sites/default/files/images/693952main_pia15817-full_full.jpg",
            "Orion" : "https://kottke.org/plus/misc/images/orion-nebula.jpg"
        ]
        var urls = Dictionary<String,URL>()
        for (key, value) in NASAURLStrings {
            urls[key] = URL(string: value)
        }
        return urls
    }()
}
